import { Component, OnInit, ViewChild } from '@angular/core';
import { ItemsService } from '../items.service';
import { Observable } from 'rxjs';
import { Items } from '../items/items';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '../auth/auth.service';
declare var $: any;
@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {
  totalRecords : string;
  page : number = 1;
  productType= '';
  productName = '';
  productPrice = '0';
  productAmount = '0';
  productDescription = '';
  productId = '';
  // dtOptions: DataTables.Settings = {};
  item: Items;
  searchProduct = '';
  constructor(public itemService: ItemsService, private authService:AuthService) { }
  itemList: Items[];
  productSearchFinal = [];
  mailLogin : string;
  ngOnInit() {
    this.mailLogin = localStorage.getItem("mail")
  this.getAllProduct();
  // this.getAllProductByAPI();
}
signOut = () => {
  this.authService.signOut();
}

  // getAllProductByAPI(){
  //   this.itemService.getItems().subscribe(data => {
  //       data.map(e => {
  //         console.log(e)
  //         this.itemList.push(e)
  //       })
  //     });
  //    console.log(this.itemList)
  //   }

  // CRUD

  getAllProduct(){
    this.itemService.getItemsFirebase('items').subscribe(data => {
      this.itemList = data.map(e => {
        return {
          productId: e.payload.doc.data()['productId'],
          productName: e.payload.doc.data()['productName'],
          productPrice: e.payload.doc.data()['productPrice'],
          productAmount: e.payload.doc.data()['productAmount'],
          productDescription: e.payload.doc.data()['productDescription'],
  
        } as Items;
        
      })
    });
  }

  getProductToUpdate(){
    this.getAllProduct();
  }

  onSetProductName(event: KeyboardEvent) { // with type info
    this.productName = (event.target as HTMLInputElement).value;
  }

  onSetProductPrice(event: KeyboardEvent) {
    this.productPrice = (event.target as HTMLInputElement).value;
  }

  onSetProductType(type) {
    this.productType = type;
  }

  onSetProductAmount(event: KeyboardEvent) {
    this.productAmount = (event.target as HTMLInputElement).value;
  }

  onSetProductDescription(event: KeyboardEvent) {
    this.productDescription = (event.target as HTMLInputElement).value;
  }

  create() {
    try {
      const item = {
        productId: this.productId,
        productName: this.productName,
        productAmount: parseInt(this.productAmount),
        productPrice: parseInt(this.productPrice),
        productDescription: this.productDescription,
        productType: this.productType,
        visible: true,
      };
      const response = this.itemService.createItem(item, 'items');
      if (response !== undefined) {
        window.alert("Success")
        this.getAllProduct()
        this.resetDataTable()

        console.log(response)
      }
    }
    catch (error) {
      window.alert(error.message)
    }
  }

  insertByApi = () => {
    try {
      const item = {
        productId: this.productId,
        productName: this.productName,
        productAmount: parseInt(this.productAmount),
        productPrice: parseInt(this.productPrice),
        productDescription: this.productDescription,
        productType: this.productType,
        visible: false
      };
      const response = this.itemService.insertItemAPIService(item).subscribe(data => {
        console.log(data)
        // this.getProductToUpdate()
      });
      if (response !== undefined) {
        console.log(response)
        this.resetDataTable()
        window.alert("Success")
      }
    }
    catch (error) {
      window.alert(error.message)
    }
  }
  deleteAPI = () => {
    try {
      this.itemService.deleteItemAPIService(this.productId).subscribe(data => {
        console.log(data)
        window.alert("success");
        
        // this.getProductToUpdate()

      });
    }
    catch (error) {
      window.alert(error.message)
    }

  }

  resetDataTable = () => {
    this.productId = '';
    this.productName = '';
    this.productDescription = '';
    this.productPrice = '';
    this.productAmount = '';
  }

  getItemFromDataTable = (item: any) => {
    this.productId = item.productId;
    this.productName = item.productName;
    this.productDescription = item.productDescription;
    this.productPrice = item.productPrice;
    this.productAmount = item.productAmount;
  }

  updateAPI() {
    try {
      const item = {
        productId: this.productId,
        productName: this.productName,
        productAmount: parseInt(this.productAmount),
        productPrice: parseInt(this.productPrice),
        productDescription: this.productDescription,
        visible: true,
      };
      console.log(item)
      this.itemService.updateItemAPIService(item).subscribe(data => {
        console.log(data)
        window.alert("update Success")

      });
    }
    catch (error) {
      window.alert(error.message);
    }
  }

  update() {
    try {
      const item = {
        productId: this.productId,
        productName: this.productName,
        productAmount: parseInt(this.productAmount),
        productPrice: parseInt(this.productPrice),
        productDescription: this.productDescription,
        visible: true,
      };
      const response = this.itemService.updateitem(item, 'items');
      if (response !== undefined) {
        window.alert("update Success")
        this.getAllProduct()

      }
    }
    catch (error) {
      window.alert(error.message);
    }
  }

  delete = () => {
    try {
      const response = this.itemService.deleteItem(this.productId, 'items');
      if (response !== undefined) {
        window.alert("Delete success")
        this.getAllProduct()

      }
    }
    catch (error) {
      window.alert(error.message)
    }

  }

  getProductByType = (type) => {
    try {
      const response = this.itemService.getItemsByType(type, 'items');
      if (response !== undefined) {
        console.log(response);
        this.itemList = response;
        window.alert("get success")
      }
    }
    catch (error) {
      window.alert(error.message)
    }

  }

  //Search
  
  onSetSearchProduct(event: KeyboardEvent) { 
    this.searchProduct = (event.target as HTMLInputElement).value;
    console.log(this.searchProduct)
    this.productSearchFinal = this.productsFound();
    console.log(this.productSearchFinal)
  }

  
   escapeRegExp(str) {
    return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
  }

  comp = (a, b) => a.toLowerCase().trim() === b.toLowerCase().trim();

  itemFound =  () => {
    return this.findProductBy(this.searchProduct, this.itemList);
  }

  productsFound = () => {
    let itemTemp = this.itemFound()
    return itemTemp.length === 1 && 
    this.comp(this.searchProduct, itemTemp[0].productName) ? [] : itemTemp
  }

  findProductBy(queryItem, items) {
    console.log(items)
    console.log(queryItem)
    if (queryItem === '') {
        return [];
    }
    var regEscape = this.escapeRegExp(queryItem)
    const regex = new RegExp(`${regEscape.trim()}`, 'i');
    return items.filter(dataItem =>
        dataItem.productName.search(regex) >= 0);
  }

}

