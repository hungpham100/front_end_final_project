import { NgModule } from '@angular/core';
import { Routes, RouterModule, CanActivate } from '@angular/router';
import {ItemsComponent} from './items/items.component'
import {LoginComponent} from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import {ProductComponent} from './product/product.component';
import {AuthGuardServiceService} from './auth/auth-guard-service.service';
const routes: Routes = [
 { path: '', redirectTo: '/admin', pathMatch: 'full' },
  { path: 'admin', component: ItemsComponent , canActivate: [AuthGuardServiceService]},
  // { path: 'admin', component: ItemsComponent },
  { path: 'login', component: LoginComponent},
  { path: 'signup', component: SignupComponent},
  { path: 'product', component: ProductComponent},
  
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
