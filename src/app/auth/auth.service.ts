import { Injectable, NgZone } from '@angular/core';
import { Router } from '@angular/router';
import { User } from './user.model';

import { auth } from 'firebase/app';
import 'firebase/auth';
import * as firebase from 'firebase';

import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';

import { Observable, of } from 'rxjs';
import { switchMap } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class AuthService {

  user: User;

    constructor(
      public router: Router,
      public ngZone: NgZone,
      public afAuth: AngularFireAuth,
      private angularFireAuth: AngularFireAuth
  ) {
      this.afAuth.authState.subscribe(user => {
          this.user = user;
      })
     }
   // Firebase SignInWithPopup
   async OAuthProvider(provider) {
    try {
       const res = await this.afAuth.signInWithPopup(provider);
       this.ngZone.run(() => {
         this.router.navigate(['dashboard']);
       });
     }
     catch (error) {
       window.alert(error);
     }
}

// Sign up with email/password
signUpByAccount(email, password) {
  return this.afAuth.createUserWithEmailAndPassword(email, password)
    .then((result) => {
      window.alert("You have been successfully registered!");
    }).catch((error) => {
      window.alert(error.message)
    })
}

// Sign in with email/password
signInByAccount(email, password) {
  return this.afAuth.signInWithEmailAndPassword(email, password)
    .then((result) => {
        this.router.navigate(['admin']);
        localStorage.setItem("mail",email);
    }).catch((error) => {
      window.alert(error.message)
    })
}
// Firebase Google Sign-in
  async signinWithGoogle() {
    try {
    const res = await this.OAuthProvider(new auth.GoogleAuthProvider());
    console.log(res);
  }
  catch (error) {
    console.log(error);
  }
}

// Sign in with Google
GoogleAuth() {
  return this.AuthLogin(new auth.GoogleAuthProvider());
}  

// Auth logic to run auth providers
AuthLogin(provider) {
  return this.afAuth.signInWithPopup(provider)
  .then((result) => {
      console.log('You have been successfully logged in!')
  }).catch((error) => {
      console.log(error)
  })
}
// Firebase Logout 
signOut() {
    return this.afAuth.signOut().then(() => {
        this.router.navigate(['login']);
       localStorage.removeItem('mail');
    })
}

isValid = () => {
  return localStorage.getItem('checkLogin') === "true" && 
  localStorage.getItem('checkLogout') === 'true'? false : true
}

getCurrentUser() {
  return new Promise<any>((resolve, reject) => {
      var user = firebase.auth().onAuthStateChanged(function (user) {
          if (user) {
              // this.cookies.set("email", user.email)
              resolve(user);
          } else {
              reject('Please login');
          }
      })
  })
}
}