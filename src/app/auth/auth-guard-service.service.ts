import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AuthService } from './auth.service';
import { Observable } from 'rxjs';

@Injectable()
export class AuthGuardServiceService implements CanActivate {

  constructor(private authentication: AuthService, private router: Router) {}
  
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return new Promise((resolve, reject) => {
        this.authentication.getCurrentUser()
            .then(user => {
                resolve(true)

            },
                err => {
                    resolve(false);
                    alert(err)
                    window.location.href = "/login"

                    //nếu chưa đăng nhập chuyển sang trang login
                })
    })
    }
}