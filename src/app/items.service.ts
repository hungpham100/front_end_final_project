import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Items } from 'src/app/items/items';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Item } from './app.component';
@Injectable({
  providedIn: 'root'
})
export class ItemsService {

  constructor(private http: HttpClient, private firestore: AngularFirestore) {

  }

  getItemsFirebase(child) {
    return this.firestore.collection(child).snapshotChanges();
  }

  insertItemAPIService(item: Items): Observable<Items> {
    const newId = this.firestore.createId();
    item.productId = newId;
    return this.http.post<Items>('https://front-end-project-8985a.web.app/api/v1/items', item);
  }

  updateItemAPIService(item: Items): Observable<Items> {
    return this.http.patch<Items>('https://front-end-project-8985a.web.app/api/v1/updateItems/'+item.productId, item);
  }

  deleteItemAPIService(id: any): Observable<Items> {
    return this.http.delete<Items>('https://front-end-project-8985a.web.app/api/v1/deleteItems/'+id);
  }

  createItem(item: any, child) {
    const newId = this.firestore.createId();
    item.productId = newId
    return this.firestore.collection(child).doc(item.productId).set(item);
  }

  updateitem(item: any, child){
    // delete item.id;
    return this.firestore.doc(child + '/' + item.productId).update(item);
  }

  deleteItem(itemId: string, child) {
    this.firestore.doc(child + '/' + itemId).delete();
  }
  getItems(): Observable<Items[]> {
    return this.http.get<Items[]>('https://front-end-project-8985a.web.app/api/v1/items');

  }

  getItemsByType = (type:any, child:any) => {
    let res = [];
    let citiesRef =this.firestore.collection(child);
    let query = citiesRef.ref.where('productType', '==', type).get()
  .then(snapshot => {
    if (snapshot.empty) {
      console.log('No matching documents.');
      return;
    }  

    snapshot.forEach(doc => {
      console.log(doc.id, '=>', doc.data());
      res.push(doc.data())
    });
  })
  .catch(err => {
    res = err.message
  });

  return res
    
  }
 
}

