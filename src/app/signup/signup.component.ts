import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth/auth.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  email = '';
  pass = '';
  confirmPass = '';
  username = '';
  checkConfirm = false;
  constructor(public authSerivce: AuthService) { }

  ngOnInit(): void {
  }
  onEmail(event: KeyboardEvent) { // with type info
    this.email = (event.target as HTMLInputElement).value;
  }

  onPassword(event: KeyboardEvent) {
    this.pass = (event.target as HTMLInputElement).value;
  }

  onConfirmPass(event: KeyboardEvent) {
    this.confirmPass = (event.target as HTMLInputElement).value;
    this.onCheckConfirmPassword();
  }

  onCheckConfirmPassword = () => {
      if(this.pass===this.confirmPass){
        this.checkConfirm = false;
      }
      else{
        this.checkConfirm = true;
      }
  }

  signUp = () => {
    if(this.checkConfirm===false){
      this.authSerivce.signUpByAccount(this.email, this.pass);
    }
  }
}
