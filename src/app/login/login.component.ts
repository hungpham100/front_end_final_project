import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth/auth.service';
import { Router, ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  email = '';
  password = '';
  return: string = '';
 
  constructor(public authService : AuthService,
    private router: Router,
    private route: ActivatedRoute,
    ) {};

  ngOnInit() {
    
    console.log(localStorage)
    this.route.queryParams
    .subscribe(params => this.return = params['return'] || '/admin');
  }
  onEmail(event: KeyboardEvent) { 
    this.email = (event.target as HTMLInputElement).value;
  }

  onPassword(event: KeyboardEvent) {
    this.password = (event.target as HTMLInputElement).value;
  }

  signIn = () => {
     if (this.email && this.password) {
      this.authService.signInByAccount(this.email,this.password);
    }

  }

  removeStorage = () => {
    localStorage.removeItem('checkLogin');
    localStorage.removeItem('checkLogout');
  }

}
