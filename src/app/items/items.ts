// export class Items {
//     public constructor(public id: string,public name: string) {
//     }
//     }


    export class Items {
        productId: string;
        productName: string;
        productPrice: number;
        productAmount: number;
        productDescription: string;
        visible: boolean;
    }