// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyCd7mIqVwzg8cMidkfArlRd61-wOpJjDtc",
    authDomain: "front-end-project-8985a.firebaseapp.com",
    databaseURL: "https://front-end-project-8985a.firebaseio.com",
    projectId: "front-end-project-8985a",
    storageBucket: "front-end-project-8985a.appspot.com",
    messagingSenderId: "1018316335100",
    appId: "1:1018316335100:web:690a4c5475a77b9ba2f479",
    measurementId: "G-DZEMG3X5FC"
  }
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
